---
title: "{{ replace .Name "-" " " | title }}""
date: "{{.Date }}"
draft: true
type: interview
audio:
  name: {{ .Name }}
  date:
  formats:
    - webm
    - mp4
---

