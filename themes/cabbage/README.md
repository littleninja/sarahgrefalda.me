# Cabbage Hugo Theme

A minimalist theme for the privacy-conscious technologist to blog and share projects.

// screenshot

// live demo

## Features

Cabbage extends the base [Ananke](https://github.com/budparr/gohugo-theme-ananke) Hugo theme. It includes the following features:

- Responsive design
- Accessible design
- Dark theme using browser settings and toggle
- i18n support
- Multilingual support
- Contact form
- Pagination
- Table of contents
- RSS discovery
- Site metadata for SEO
- Site map
- Webmention.io for mentions and pingbacks
- [Netlify](https://www.netlify.com/) configuration for deployment

This theme uses [Tailwind CSS](https://tailwindcss.com/). To edit the configuation, see Advanced features.

## Prerequisites

- Hugo 0.74 or later
- Node.js latest long-term support (LTS) version and npm

## Installation

Install the Cabbage theme as a submodule. From the root directory of your Hugo site, run the following:

```
   $ git submodule add https://gitlab.com/littleninja/gohugo-theme-cabbage.git themes/cabbage
```

For more information read the official [setup guide](//gohugo.io/overview/installing/) of Hugo.

## Getting started

// todo, copy config.toml?

## Advanced

### Modifying Tailwind CSS configuration

Tailwind gives a set of utility classes so you can apply atomic CSS styles directly in your HTML markup. Including all of these variants would be [_huge_](https://tailwindcss.com/docs/optimizing-for-production) (like 3739.4kB uncompressed huge at time of writing) and that's not its intended use.

You may need to rebuild styles if you use new classes in your layouts. This will be taken care of for you when deployed to Netlify as a build step. (todo)

#### `src/tailwind.config.js`

The following are examples of configuration changes you can apply. For more details, see the official [Tailwind CSS Configuration](https://tailwindcss.com/docs/configuration) docs.

- Help Tailwind find additional files when purging so your classes are included in the production stylesheet (`purge`).
- Configure [theme](https://tailwindcss.com/docs/theme) to modify or extend the default.
- Configure core plugins to always include or exclude sets of classes.

#### `src/css/_tailwind.css`

The following are examples of changes you can apply directly to the base stylesheet.

- Undoing `preflight` reset styles
- Adding global styles using Tailwind `@apply` directive or otherwise

## Production

Some features like analytics are only included when run in production. To run in production set the environment variable `HUGO_ENV=production` before your build command. For example:

```shell
   $ HUGO_ENV=production hugo

   # for Windows OS
   $ set HUGO_ENV=production
   $ hugo
```

## Contributing

If you find a bug or have an idea for a feature, feel free to use the [issue tracker](https://gitlab.com/littleninja/gohugo-theme-cabbage/-/issues) to let me know.
