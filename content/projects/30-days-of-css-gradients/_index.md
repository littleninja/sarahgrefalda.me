---
title: "30 Days of CSS Gradients"
date: 2021-11-04T19:08:37-07:00
description: "A challenge to design CSS backgrounds with gradients for 30 days"
featured_image: ""
layout: 30-days-of-css-gradients
---

A new CSS background for 30 days.

Some goals:

- **creativity** : look for inspiration in the ordinary, apply CSS creatively to create the pattern, color!
- **math** : rediscover geometry 📐❤️
- **make** : build something new every day, share it, ignore perfection, try it first before looking at a solution

<!-- 2021-12-04 -->
{{< css-card bg-css="background: radial-gradient(ellipse at top right, transparent 78%, rgba(255, 255, 255, 0.32)), radial-gradient(ellipse at bottom left, transparent 78%, rgba(255, 255, 255, 0.32)), linear-gradient(#07034F, #17024E, #26024D);" text="text-white" title="30. fin" aria-description="dark blue and purple background with soft white glow on opposite corners">}}

<!-- 2021-12-04 -->
{{< css-card bg-css="--day-29-b1a: #826357; --day-29-b1b: #A68072; --day-29-b1c: #533E36; --day-29-b1d: #EAECEC; --day-29-b2a: white; --day-29-b2b: black; --day-29-b2c: #536674; --day-29-sky-start: #028EB5; --day-29-sky-end: #06BBEE; background: conic-gradient(from 76deg at 40% 25%, var(--day-29-b1d) 87deg, var(--day-29-b2b) 87deg 102deg, var(--day-29-b2c) 109deg, transparent 109deg), linear-gradient(188deg, transparent 10%, white 10% 12%, rgba(255, 255, 255, 0.32)) 9rem 9rem / 20% 100% no-repeat no-repeat, linear-gradient(var(--day-29-sky-start), var(--day-29-sky-end) 50%) 9rem 0 / 100% 100% no-repeat no-repeat, conic-gradient(from -82deg at 10% 40%, var(--day-29-b1d) 90deg, var(--day-29-b1a) 90deg 135deg, var(--day-29-b1b) 255deg, var(--day-29-b1c) 255deg);" text-css="background: rgba(255, 255, 255, 0.67);" text="text-black" title="29. cube city" aria-description="view from underneath two windowless cube buildings, one brown and white on the left and one black and white on the right, against a clear blue sky">}}

<!-- 2021-12-03 -->
{{< css-card bg-css="--day-28-c1: #FFFCF4; --day-28-c2: #E9E4D3; --day-28-c3: #B8B29F; background: radial-gradient(ellipse, transparent, rgba(255, 255, 255, 0.54)), radial-gradient(ellipse, transparent, rgba(255, 255, 255, 0.67)) 0 0 / 12rem 7rem, conic-gradient(var(--day-28-c1) 60deg, var(--day-28-c3) 60deg 120deg, var(--day-28-c2) 120deg 180deg, var(--day-28-c1) 180deg 240deg, var(--day-28-c2) 240deg 300deg, var(--day-28-c3) 300deg) 50% 50% / 12rem 7rem;" text="text-black" title="28. origami" aria-description="folded triangle patterns into paper with hues of soft white, cream, and beige">}}

<!-- 2021-12-03 -->
{{< css-card bg-css="background: repeating-linear-gradient(-45deg, transparent, rgba(255, 255, 255, 0.12) 5%, transparent 10%), repeating-linear-gradient(45deg, rgba(255, 255, 255, 0.12), transparent 5%, rgba(255, 255, 255, 0.12) 10%), radial-gradient(circle at 72% 24%, #FA404E, #EB0011);" text="text-black" title="27. elf stocking" aria-description="red background with soft white chainlink motif">}}

<!-- 2021-12-01 -->
{{< css-card bg-css="--day-26-c1: #00A44C; --day-26-c2: #00937A; --day-26-c3: #005025; --day-26-c4: #F23056; --day-26-c5: #AD0022; --day-26-c6: #8F001C; background: radial-gradient(circle at bottom right, var(--day-26-c4), var(--day-26-c5) 14%, var(--day-26-c6) 17%, transparent 17%), linear-gradient(145deg, black, black 3%, white 3% 6%, black 6% 9%, white 9% 12%, black 12% 15%, white 15% 18%, black 18% 21%, transparent 21%) top left, linear-gradient(16deg, rgba(0, 0, 0, 0.24) 22%, transparent 22%), linear-gradient(45deg, var(--day-26-c3) 20%, var(--day-26-c1) 20% 80%, var(--day-26-c2) 80%); background-repeat: no-repeat;" text="text-black" title="26. choreography" aria-description="bold shades of green through the middle, black and white stripes at the top left corner, and bright red ball at the bottom right corner">}}

<!-- 2021-11-30 -->
{{< css-card bg-css="--day-25-c1: #FFEEB1; --day-25-c2: #FFC347; --day-25-c3: #FF8900; background: conic-gradient(from -38deg at 18% 82%, var(--day-25-c1), var(--day-25-c1) 60deg, var(--day-25-c2) 60deg 120deg, var(--day-25-c3) 120deg 180deg, var(--day-25-c1) 180deg 240deg, var(--day-25-c2) 240deg 300deg, var(--day-25-c3) 300deg); background-repeat: no-repeat;" text="text-black" title="25. umbrella" aria-description="view looking up inside an umbrella with orange, yellow, and white panels">}}

<!-- 2021-11-29 -->
{{< css-card bg-css="--day-24-bg: #18015D; --day-24-box: #21027E; background: linear-gradient(rgba(255, 255, 255, 0.87), rgba(255, 255, 255, 0.32)) center calc(50% + 3rem) / 6rem 0.2rem, linear-gradient(var(--day-24-box), var(--day-24-box)) center / 6rem 6rem, linear-gradient(90deg, transparent 30%, black 55%, transparent 65%) center / 100% 5.4rem, conic-gradient(from 58deg at calc(50% - 1.5rem) center, transparent, var(--day-24-bg) 4deg, var(--day-24-bg) 62deg, transparent 66deg), conic-gradient(from 236deg at calc(50% + 1.5rem) center, transparent, var(--day-24-bg) 4deg, var(--day-24-bg) 62deg, transparent 66deg), radial-gradient(circle, rgba(252, 255, 0, 0.12) 34%, transparent), radial-gradient(circle, rgba(255, 255, 255, 0.54) 5%, rgba(255, 255, 0, 0.12) 24%, transparent 36%), var(--day-24-bg); background-repeat: no-repeat;" text-css="letter-spacing: 0.4em;" text="text-white" title="24. nightlight" aria-description="a square wall nightlight glowing on a dark, purple wall">}}

<!-- 2021-11-28 -->
{{< css-card bg-css="--day-23-pink-md: #F985C7; --day-23-pink-dk: #E9399D; --day-23-pink-lt: #FFF2F9; --day-22-pulp: #FF8F32; background: linear-gradient(27deg, transparent 5%, var(--day-23-pink-md) 5% 16%, var(--day-23-pink-lt) 16% 32%, transparent 32%), linear-gradient(-54deg, transparent 5%, var(--day-23-pink-dk) 5% 12%, var(--day-23-pink-lt) 12% 26%, transparent 26%), linear-gradient(168deg, transparent 5%, var(--day-23-pink-lt) 5% 19%, var(--day-23-pink-dk) 19% 31%, transparent 31%), #82F4F4;" text="text-black" title="23. triangle" aria-description="a modern and playful triangular prism set asymmetrically, points just out of view, over a pastel blue background">}}

<!-- 2021-11-28 -->
{{< css-card bg-css="--day-22-bg: #1A237E; --day-22-star: #283593; --day-22-sz: 8rem; background: linear-gradient(-32deg, var(--day-22-bg) 1rem, transparent 1rem) 4rem 4rem / var(--day-22-sz) var(--day-22-sz), linear-gradient(32deg, var(--day-22-bg) 1rem, transparent 1rem) 4rem 4rem / var(--day-22-sz) var(--day-22-sz), linear-gradient(-73deg, var(--day-22-star) 1rem, transparent 1rem) 4rem 4rem / var(--day-22-sz) var(--day-22-sz), linear-gradient(73deg, var(--day-22-star) 1rem, transparent 1rem) 4rem 4rem / var(--day-22-sz) var(--day-22-sz), linear-gradient(-143deg, var(--day-22-star) 0.9rem, transparent 0.9rem) 4rem 1.7rem / var(--day-22-sz) var(--day-22-sz), linear-gradient(143deg, var(--day-22-star) 0.9rem, transparent 0.9rem) 4rem 1.7rem / var(--day-22-sz) var(--day-22-sz), linear-gradient(-32deg, var(--day-22-bg) 1rem, transparent 1rem) 8rem 8rem / var(--day-22-sz) var(--day-22-sz), linear-gradient(32deg, var(--day-22-bg) 1rem, transparent 1rem) 8rem 8rem / var(--day-22-sz) var(--day-22-sz), linear-gradient(-73deg, var(--day-22-star) 1rem, transparent 1rem) 8rem 8rem / var(--day-22-sz) var(--day-22-sz), linear-gradient(73deg, var(--day-22-star) 1rem, transparent 1rem) 8rem 8rem / var(--day-22-sz) var(--day-22-sz), linear-gradient(-143deg, var(--day-22-star) 0.9rem, transparent 0.9rem) 8rem 5.7rem / var(--day-22-sz) var(--day-22-sz), linear-gradient(143deg, var(--day-22-star) 0.9rem, transparent 0.9rem) 8rem 5.7rem / var(--day-22-sz) var(--day-22-sz), var(--day-22-bg);" text="text-white" title="22. stars again" aria-description="an even pattern of blue stars on a dark indigo sky">}}

<!-- 2021-11-28 -->
{{< css-card bg-css="--day-21-lattice: #E64A19; background: repeating-linear-gradient(transparent, transparent 0.5rem, var(--day-21-lattice) 0.5rem 0.55rem, transparent 0.55rem 0.65rem, var(--day-21-lattice) 0.65rem 0.7rem), repeating-linear-gradient(-80deg, transparent, transparent 0.75rem, var(--day-21-lattice) 0.75rem 0.85rem), repeating-linear-gradient(80deg, transparent, transparent 0.75rem, var(--day-21-lattice) 0.75rem 0.85rem), radial-gradient(circle at 70% 30%, #FF9B48, #EA6A00 90%);" text="text-black" title="21. oranges" aria-description="orange glow behind a thin red lattice netting">}}

<!-- 2021-11-27 -->
{{< css-card bg-css="--day-20-bg: #388E3C; --day-20-tree: #66BB6A; --day-20-trunk: #4CAF50; background: linear-gradient(transparent 70%, rgba(27, 94, 32, 0.54)), linear-gradient(90deg, transparent 48%, var(--day-20-tree) 48% 52%, transparent 52%) 0 4rem / 4rem 3rem, linear-gradient(60deg, var(--day-20-tree) 1rem, transparent 1rem) 0 4rem / 4rem 3rem, linear-gradient(-60deg, var(--day-20-tree) 1rem, transparent 1rem) 0 4rem / 4rem 3rem, repeating-linear-gradient(var(--day-20-bg), var(--day-20-bg) 1rem, transparent 1rem 1.5rem, var(--day-20-bg) 1.5rem 3rem), linear-gradient(90deg, var(--day-20-trunk) 4%, transparent 4% 96%, var(--day-20-trunk) 96%) 0 4rem / 4rem 3rem, var(--day-20-bg);" text-css="" text="text-black" title="20. tree" aria-description="geometric evergreen tree in holiday green">}}

<!-- 2021-11-25 -->
{{< css-card bg-css="--day-19-hex: #FFF176; --day-19-rho: #FFF9C4; background: linear-gradient(120deg, var(--day-19-rho) 2.07rem, #FFFDE7 2.07rem 2.27rem, transparent 2.27rem 9.90rem, #FFEB3B 9.90rem 10.10rem, var(--day-19-rho) 10.10rem) 2.5rem 0 / 9.0rem 8.66rem, linear-gradient(60deg, var(--day-19-rho) 2.07rem, var(--day-19-hex) 2.07rem 10.0rem, var(--day-19-rho) 10.0rem) 2.5rem 0 / 9.0rem 8.66rem, lightyellow;" text-css="" text="text-black" title="19. hexagon" aria-description="a pattern of hexagons and rhombuses in cheerful light yellows">}}

<!-- 2021-11-23 -->
{{< css-card bg-css="background: linear-gradient(90deg, #eee 48%, #ddd 50%, #eee 50%);" text-css="background: black;" text="text-white" title="18b. cornsweet" aria-description="visual illusion using a thin gradient to make one side appear darker, placing a pencil or pen over the gradient cancels the effect">}}

<!-- 2021-11-23 -->
{{< css-card bg-css="background: linear-gradient(90deg, transparent 43%, blue 43% 44%, transparent 44%), linear-gradient(90deg, transparent 56%, blue 56% 57%, transparent 57%), repeating-conic-gradient(transparent, transparent 5deg, black 5deg 6deg),  white;" text-css="background: red;" text="text-black" title="18a. hering" aria-description="visual illusion created by lines like bicycle spokes with two blue lines placed near the center vertically that appear bowed">}}

<!-- 2021-11-22 -->
{{< css-card bg-css="background: url('data:image/svg+xml;utf8,<svg xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 100 100\"><rect height=\"70\" fill=\"white\" x=\"2\" y=\"20\" width=\"4\"></rect><rect width=\"70\" fill=\"white\" x=\"20\" y=\"2\" height=\"4\"></rect></svg>') 0.75rem 0.75rem / 2.5rem 2.5rem repeat, radial-gradient(circle, transparent 70%, #000), repeating-radial-gradient(circle, #000, #212121 0.4rem);" text-css="background: red;" text="text-black" title="17. ehrenstein again*" aria-description="visual illusion created by a small grid of non-intersecting lines with a radial gradient to break the illusion: no black circles at the intersection points">}}

<!-- 2021-11-21 -->
{{< css-card bg-css="background: linear-gradient(grey, grey 0.1rem, transparent 0.1rem 50%, grey 50% calc(50% + 0.1rem), transparent calc(50% + 0.1rem)), conic-gradient(white, white 90deg, transparent 90deg 270deg, black 270deg), linear-gradient(90deg, black, black 40%, white 40% 90%, black 90%); background-size: 4rem 4rem;" text-css="background: red;" text="text-black" title="16. cafe wall" aria-description="visual illusion created by parallel rows of black and white tiles slightly misaligned vertically, creating an off-balance effect on the rows">}}

<!-- 2021-11-20 -->
{{< css-card bg-css="background: radial-gradient(circle at center, black, black 0.5rem, transparent 0.5rem), linear-gradient(transparent calc(50% - 0.05rem), white calc(50% - 0.05rem) calc(50% + 0.05rem), transparent calc(50% + 0.05rem)), linear-gradient(0.25turn, transparent calc(50% - 0.05rem), white calc(50% - 0.05rem) calc(50% + 0.05rem), transparent calc(50% + 0.05rem)), black; background-size: 4.25rem 4.25rem;" text-css="background: red;" text="text-black" title="15. ehrenstein*" aria-description="visual illusion created by a high-contrast grid with gaps at the intersection points">}}

<!-- 2021-11-19 -->
{{< css-card bg-css="background: linear-gradient(145deg, rgba(2, 119, 189, 0.67), rgba(2, 119, 189, 0.12), rgba(2, 119, 189, 0.67)), repeating-linear-gradient(0.125turn, #B3E5FC, #B3E5FC 2rem, #81D4FA 2rem 4rem);" text-css="" text="text-black" title="14. stripes" aria-description="diagonal light blue lines with a shadow on each side">}}

<!-- 2021-11-18 -->
{{< css-card bg-css="--day-13-green-lightest: rgba(169, 226, 173, 1); --day-13-green-light: rgba(119, 204, 125, 1); --day-13-green-dark: rgba(45, 148, 52, 1); --day-13-green-darkest: rgba(21, 120, 27, 1); background: linear-gradient(45deg, rgba(21, 120, 27, 0.54), rgba(169, 226, 173, 0.54)), linear-gradient(var(--day-13-green-lightest), var(--day-13-green-lightest) 14.3%, var(--day-13-green-light) 14.3% 28.6%, var(--day-13-green-darkest) 28.6% 42.9%, var(--day-13-green-dark) 42.9% 57.2%, var(--day-13-green-light) 57.2% 71.5%, var(--day-13-green-darkest) 71.5% 85.8%, var(--day-13-green-lightest) 71.5% 85.8%);" text-css="" text="text-black" title="13. streak" aria-description="green stripes similar to GitHub contribution streak graph">}}

<!-- 2021-11-17 -->
{{< css-card bg-css="--day-12-navy: rgba(12, 27, 120, 0.67); --day-12-navy-50: rgba(12, 27, 120, 0.34); --day-12-teal: rgba(45, 189, 184, 1); --day-12-white: rgba(255, 255, 255, 0.34); background: repeating-linear-gradient(0.25turn, var(--day-12-navy), var(--day-12-navy) 1rem, var(--day-12-white) 1rem 1.5rem, var(--day-12-navy) 1.5rem 1.75rem, var(--day-12-navy-50) 1.75rem 2rem, var(--day-12-white) 2rem 2.25rem, var(--day-12-navy-50) 2.25rem 2.5rem, var(--day-12-navy) 2.5rem 2.75rem, var(--day-12-white) 2.75rem 3.25rem, var(--day-12-navy) 3.25rem 4.25rem, transparent 4.25rem 5rem, var(--day-12-navy) 5rem 5.75rem, transparent 5.75rem 7rem), repeating-linear-gradient(0.5turn, var(--day-12-navy), var(--day-12-navy) 1rem, var(--day-12-white) 1rem 1.5rem, var(--day-12-navy) 1.5rem 1.75rem, var(--day-12-navy-50) 1.75rem 2rem, var(--day-12-white) 2rem 2.25rem, var(--day-12-navy-50) 2.25rem 2.5rem, var(--day-12-navy) 2.5rem 2.75rem, var(--day-12-white) 2.75rem 3.25rem, var(--day-12-navy) 3.25rem 4.25rem, transparent 4.25rem 5rem, var(--day-12-navy) 5rem 5.75rem, transparent 5.75rem 7rem), var(--day-12-teal);" text-css="background: rgba(12, 27, 120, 1)" text="text-white" title="12. work shirt" aria-description="blue teal and white plaid pattern">}}

<!-- 2021-11-16 -->
{{< css-card bg-css="background: repeating-radial-gradient(circle, rgba(213, 0, 0, 1) 20%, rgba(255, 23, 68, 0.54) 40%, transparent 40%), radial-gradient(circle, rgba(62, 39, 35, 1) 25%, rgba(221, 44, 0, 1) 35%), #BF360C;" text-css="" text="text-white" title="11. looney" aria-description="telescoping red ripples from Looney Tunes introduction">}}

<!-- 2021-11-16 -->
{{< css-card bg-css="background: radial-gradient(circle, #E0F2F1 0.7rem, transparent 0.7rem) 0 0, radial-gradient(circle, #B2DFDB 0.5rem, transparent 0.5rem) 0.6rem -0.6rem, radial-gradient(circle, #B2DFDB 0.5rem, transparent 0.5rem) 0.6rem 0.6rem, radial-gradient(circle, #B2DFDB 0.5rem, transparent 0.5rem) -0.6rem 0.6rem, radial-gradient(circle, #B2DFDB 0.5rem, transparent 0.5rem) -0.6rem -0.6rem, radial-gradient(circle, #E0F2F1 0.9rem, transparent 0.9rem) 0 1rem, radial-gradient(circle, #E0F2F1 0.9rem, transparent 0.9rem) 0 -1rem, radial-gradient(circle, #E0F2F1 0.9rem, transparent 0.9rem) -1rem 0, radial-gradient(circle, #E0F2F1 0.9rem, transparent 0.9rem) 1rem 0, radial-gradient(circle, #B2DFDB 2.1rem, #E0F2F1 2.1rem 2.5rem, #80CBC4 2.5rem); background-size: 4rem 4rem;" text-css="background: #E0F2F1;" text="text-black" title="10. mosaic" aria-description="light teal tesalation of a circular floral design with rounded diamonds between">}}

<!-- 2021-11-14 -->
{{< css-card bg-css="background: radial-gradient(circle, transparent 2rem, #F9FBE7 2.1rem 2.2rem, #7CB342 2.4rem 2.5rem, #FF80AB 2.5rem), repeating-conic-gradient(#D4E157 0deg 30deg, #F0F4C3 30deg 36deg); background-size: 9rem 6rem;" text-css="background: rgba(255, 255, 255, .67);" text="text-black" title="9. lime" aria-description="colorful cross-sections of limes on a bright pink background">}}

<!-- 2021-11-13 -->
{{< css-card bg-css="background: conic-gradient(from 0deg at 2rem 0, #FF3D00 120deg, transparent 120deg), conic-gradient(from -120deg at 2rem 0, #FF3D00 120deg, transparent 120deg), conic-gradient(from 60deg at 2rem 6rem, #FF3D00 120deg, transparent 120deg), conic-gradient(from -180deg at 2rem 6rem, #FF3D00 120deg, transparent 120deg), conic-gradient(from 180deg at 2rem 4rem, #FF6E40 120deg, transparent 120deg), conic-gradient(from 60deg at 2rem 4rem, #FF9E80 120deg, transparent 120deg), conic-gradient(from 0deg at 2rem 1.7rem, #FF6E40 120deg, transparent 120deg), conic-gradient(from -120deg at 2rem 1.7rem, #FF9E80 120deg, transparent 120deg), #FF3D00; background-size: 4rem 6rem;" text-css="" text="text-black" title="8. cubes" aria-description="an illusion of neatly arranged sunset orange cubes or dodecahedrons">}}

<!-- 2021-11-11 -->
{{< css-card bg-css="background: radial-gradient(circle at 4rem 6rem, #5C6BC0 1rem, #303F9F 1rem 2rem, #5C6BC0 2rem 3rem, #303F9F 3rem 4rem, transparent 4rem), radial-gradient(circle at 0 3.97rem, #5C6BC0 1rem, #303F9F 1rem 2rem, #5C6BC0 2rem 3rem, #303F9F 3rem 4rem, transparent 4rem), radial-gradient(circle at 8rem 3.97rem, #5C6BC0 1rem, #303F9F 1rem 2rem, #5C6BC0 2rem 3rem, #303F9F 3rem 4rem, transparent 4rem), radial-gradient(circle at 4rem 2rem, #5C6BC0 1rem, #303F9F 1rem 2rem, #5C6BC0 2rem 3rem, #303F9F 3rem 4rem, transparent 4rem), #303F9F; background-size: 8rem 4rem;" text-css="background: #303F9F;" text="text-white" title="7. clouds" aria-description="dark indigo-blue ripples">}}

<!-- 2021-11-10 -->
{{< css-card bg-css="background: radial-gradient(circle, #1A237E 60%, #303F9F);" text-css="" text="text-white" title="6. bowl" aria-description="peering into the shadow of a cold, deep purple bowl">}}

<!-- 2021-11-09 -->
{{< css-card bg-css="background: radial-gradient(circle at 25% 25%, #AED581 5%, #8BC34A 20%, transparent 20%), radial-gradient(circle at 75% 75%, #AED581 5%, #8BC34A 20%, transparent 20%), #689F38; background-size: 3rem 3rem;" text-css="background: #689F38;" text="text-black" title="5. peas" aria-description="green peas evenly spaced on a pea green background">}}

<!-- 2021-11-08 -->
{{< css-card bg-css="background: radial-gradient(circle at 0% 0%, transparent 0, #FFF59D 0.5rem, #FFD54F 0.5rem 1rem, #FFF59D 1rem, transparent 1.5rem), radial-gradient(circle at 100% 0%, transparent 0, #FFF59D 0.5rem, #FFD54F 0.5rem 1rem, #FFF59D 1rem, transparent 1.5rem), radial-gradient(circle at 50% 100%, transparent 0, #FFF59D 0.5rem, #FFD54F 0.5rem 1rem, #FFF59D 1rem, transparent 1.5rem), #B2EBF2; background-size: 3rem 3rem;" text-css="background: #FFF59D;" text="text-black" title="4. sunshine" aria-description="wiggly glowing yellow lines on a light blue background">}}

<!-- 2021-11-07 -->
{{< css-card bg-css="background: conic-gradient(from -162deg at 32% 56%, #0D47A1 108deg, transparent 108deg), conic-gradient(from -90deg at 39% 35%, #0D47A1 108deg, transparent 108deg), conic-gradient(from -18deg at 61% 35%, #0D47A1 108deg, transparent 108deg), conic-gradient(from 54deg at 68% 56%, #0D47A1 108deg, transparent 108deg), conic-gradient(from 126deg at 50% 66%, #0D47A1 108deg, transparent 108deg), white; background-size: 3rem 3rem;" text-css="background: #B71C1C; border-top: 0.25rem solid white; border-bottom: 0.25rem solid white;" text="text-white" title="3. stars and stripe" aria-description="white stars on a bold blue background with a red stripe through the middle">}}

<!-- 2021-11-06 -->
{{< css-card bg-css="background: repeating-linear-gradient(transparent, transparent 16px, aliceblue 16px, aliceblue 20px), repeating-linear-gradient(90deg, transparent, transparent 16px, aliceblue 16px, aliceblue 20px), white;" text-css="background: aliceblue;" text="text-black" title="2. graph paper" aria-description="white paper with light blue graph grid">}}

<!-- 2021-11-05 -->
{{< css-card bg-css="background: radial-gradient(circle, #311B92, #311B92 35%, transparent 36%, transparent) 0 0.15rem, radial-gradient(circle, #311B92, #311B92 35%, transparent 36%, transparent) 3rem 5.5rem, radial-gradient(circle, #311B92, #311B92 35%, transparent 36%, transparent) 4.25rem 1.5rem, radial-gradient(circle, #311B92, #311B92 35%, transparent 36%, transparent) 7rem 4.25rem, tan; background-size: 8rem 8rem;" text-css="background: #311B92;" text="text-white" title="1. boba tea" aria-description="purple tapioca pearls in classic black milk tea">}}

---

## Notes and attribution

**cube city**: Inspired by photo by [Bernard Hermant](https://unsplash.com/@bernardhermant?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText) on [Unsplash](https://unsplash.com/?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText).
  
**choreography**: Inspired by photo by [Malena Gonzalez Serena](https://unsplash.com/@malegs?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText) on [Unsplash](https://unsplash.com/?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText).

**ehrenstein**: Designs credited to _Visual Illusions_ by James Kingston, published by TAJ Books International LLP in 2011.

**ehrenstein again**: Revisiting the construction of the Ehrenstein illusion. The visual illusion example sets the illusion over a faint radial background to show the reader it's the mind, not eye, that sees the black (or white) circle. Unrelated while researching how to produce the design with CSS only, Jon Kantner explained short-hand `background` in a way that applies size, position, and repeat _per image_ in a way that never clicked when I read the MDN reference docs: https://css-tricks.com/drawing-images-with-css-gradients/.


```css
.card {
	background:
		<image> <position> / <size> <repeat>,
		<image> <position> / <size> <repeat>,
		<base-color>;
}
```

I settled on inline SVG to draw the illusion thanks to an article by Chris Coyier: https://css-tricks.com/lodge/svg/09-svg-data-uris/.

```css
.card {
  background: url('data:image/svg+xml;utf8,<svg ...> ... </svg>');
}
```

Ana Tudor is masterful with CSS and gradients:
- https://css-tricks.com/background-patterns-simplified-by-conic-gradients/
- another visual illusion! https://css-tricks.com/1-html-element-5-css-properties-magic/