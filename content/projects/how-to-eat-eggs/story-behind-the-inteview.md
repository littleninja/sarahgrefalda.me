---
title: "The story behind the interview"
date: 2021-08-08T23:14:00-07:00
summary: "The context around recording voice memos with my kids and building an accessible audio player."
---

Maternity leave. Everything centered around a newborn and a thought lurked: What of my two oldest? I wanted to be intentional and spend time with them, to remind them I still very much loved them.

I also _craved_ to make something, for me. Now here's a funny thought: I had just spent _months_ creating a new person and here I was just wanting to code or build something. 🤷🏼‍♀️ In a talk long before, a mentor had explained that creators build a stockpile of creations. Creators learn with each one and add it to a pile and even with discards. This is why Jerry Seinfield's model of daily habits[^1] and GitHub contribution tracker are so powerful: we focus on making something, however imperfect, consistently and improving all the while. The pile becomes a trove for sparking new connections or a launchpoint for a new direction. My stockpile felt neglected.

Days blurred in repetition. Sip. Nurse. Snack. Rest. Diaper. Nurse. Sip. Snack. Rest. Repeat. While passing time through marathon nursing sessions I found YouTuber "How To Dad". ([Go watch his videos!](https://www.youtube.com/watch?v=UiHrA1Si0Gw)) He recorded parody how-to parenting videos with his daughters, and the optimism and playfulness was uplifting. That wasn't how I felt when I tried coding something around my kids or teaching them a new skill[^2], but then I also felt attached to the idea of quiet and focus. This sparked a new idea. Sometimes I still wanted the quiet and focus, but what if sometimes I was more flexible and invited them to be co-creators?

If we did make something together, what would it be? As the famous saying goes[^3]: “Do what you can, with what you’ve got, where you are.” I didn't have video equipment, but I had a voice memo app on my phone. I could record interviews.

Two more years would pass. Before the baby, I made a graphic illustration of an egg during a design course. During those first weeks back from maternity leave, we recorded a small collection of how-to interviews together. But it wasn't until an inspiration from accessibility that the pieces came together: closed captions[^4]. Since becoming a parent, I developed a reliance on closed captions to enjoy videos while caring for an infant. The parent life also meant my attention was distracted. Some products account for distracted users and allow us to resume with ease, and I _so_ appreciated that now. In my professional work, I was introduced to the concepts of temporary and situational abilities[^5].

It sparked the idea to build an accessible audio product.

Once again, my mentor said there was a recipe for engineers use to build products, something the engineer:
- wants to build
- wants to use
- is not yet built, or is ready for improvement

When I learned the `<audio>` element doesn't support closed caption tracks by default, I felt the need to build one. And so I did, and I hope that's what you've enjoyed. 

[^1]: I ready about Jerry Seinfield's method in one of Austin Kleon's three books, "Show Your Work!" I think.
[^2]: For the record, teaching children a new skill is hard for me: the patience, reaching past muscle memory to remember first principles, setting the bar for a new learner instead of the one I apply to myself.
[^3]: Credit to Sue Brewton for tracing the origin and correct version of the famous quote. [link to article](https://suebrewton.com/2014/12/31/squire-bill-widener-vs-theodore-roosevelt/)
[^4]: This is known as "the curb-cut effect", where groups beyond those with a disability benefit from an assistive technology. [link to article](https://mosaicofminds.medium.com/the-curb-cut-effect-how-making-public-spaces-accessible-to-people-with-disabilities-helps-everyone-d69f24c58785)
[^5]: I also learned--and am still learning--about accessibility beyond physical limits, conditions such as sensitivity to motion and light and attention deficit or hyperactivity disorders. What's remarkable is these too can have temporary and situational modes: being drunk, coping with a migraine, completing a task requiring focus with active young children around. [link to article](https://digital.gov/resources/introduction-accessibility#reframing-our-idea-of-disability)