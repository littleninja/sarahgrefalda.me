---
title: "How To Eat Eggs"
date: 2021-08-11T23:14:00-07:00
description: "An accessible audio player and an interview with my children on the proper method for eating eggs."
featured_image: "fried-egg.png"
layout: how-to-eat-eggs
---

Recorded April 19, 2019

{{< audio-caption name="how-to-eat-eggs" >}}

![A fried egg](fried-egg.png)
