---
title: "Cooking Eggs"
date: 2021-08-12T22:46:47-07:00
summary: "Just for fun. Our favorite methods for cooking eggs."
---

Eggs. They're a staple in our home. Simple and quick nourishment with surprising variety. Scrambled, fried, overeasy, soft-boiled, folded into omelets. Cheese, no cheese, yes please! Why yes, yes Sam-I-Am, we do love green eggs and ham. Of the many ways you can cook an egg, our favorite are shared below.

Memories are a funny thing. We think it's the novelties, the rare and unique moments that we'll take with us through life. But here so clearly, a common object with diverse forms sets the backdrop to these remarkably familiar moments.

There was that weekend years ago when we prepared a custard-like scramble by Gordon Ramsay. That was around the time my husband and I watched most of Hell's Kitchen and Kitchen Nightmares. There's no longer time for that sort of thing--binge watching or fussy prep.

Weekdays are a rush. It's the little things, the treat of a boiled egg, still warm, with just a pinch of salt.

Saturdays start like a deep breath of fresh air, a hopeful beginning. I step downstairs and hear the sizzle and pop of longonesa--a sweet Filipino sausage--cooking in the cast iron skillet. Steam rises from a warm pan of fried rice. To finish the spread: fried eggs or cheesy eggs, take your pick.

On mornings after a stay at my in-laws, the day starts with a traditional spread: warm rice, spam dipped in egg and fried, and sliced mango with a cup of instant cappuccino nearby. They always cook generously, there's enough for everyone to enjoy two servings. It's common to see a plate with extras through lunch for late risers and nibbling.

Anyway. Enjoy the eggs!


## Gordon Ramsay's Scrambled Eggs

Serves 1-2

- 2 eggs
- 1 Tablespoon butter
- 1 Tablespoon creme fraiche
- Pinch salt and pepper
- (optional) 1 Tablespoon fresh chives, chopped fine

In a cold and heavy sauce pan, add the eggs and butter. Set the pan on medium heat and with a heat-proof spatula, whisk and fold the eggs. As the eggs and butter just combine, about 30 seconds to 1 minute, take the pan off the heat. Using the residual heat only, continue to whisk and fold for 10 seconds before returning the pan to the heat. Repeat the cycle, 30 seconds on heat and 10 off, all while continuing to fold. Do not overcook the eggs. While the egg is still soft but starting to form "scrambles", remove from heat and season with creme fraiche, salt, pepper, and chives. Fold in the remaining ingredients and serve with toast and butter.


## Soft boiled eggs

Serves as many as you'd like

Add 5 cm water to a pot and set on medium-high heat. You want enough water so the pan won't boil dry but not so much the eggs are submerged--we're steaming them, not boiling. As the water comes to a rolling boil, carefully and gently add the eggs to the pot and cover tightly. For molten yolks, set a timer for 6 minutes (for firm yolks, 8 minutes). At the end of the timer, remove the pot from the heat and rinse eggs with cold water to stop the cooking process. Let the eggs sit in cold water another couple minutes to make them easier to handle, then peel and enjoy with a pinch of kosher salt.

Bonus: These are what E calls "eggs that EXPLODE!" 💥 If the shell cracks as they go in the water, the white escapes and looks as though the egg exploded. Or--when biting into an egg with a molten (not hard) yolk, we warn they might "explode" thanks to the mess.


## "Cheesy egg" or simple cheese omelet

Serves 2

- 4 eggs
- 1 Tablespoon milk or water
- 1/2 cup spinach, roughly torn or chopped
- 1/2 cup cheese (we like cheddar or mozzerella)
- 1 Tablespoon butter
- couple pinches of salt

In a small bowl, add eggs and milk (or water) and whisk well to combine. Set a large skillet (we use cast iron) on medium-low heat. Wait until the pan is hot, then add the butter and a couple pinches of salt to prevent the egg from sticking. Once the butter melts, add the egg mixture and gently roll the pan so the egg covers the bottom. As the edges begin to crisp and the center has bubbled, flip the egg. Add the spinach and cheese evenly over the egg, reserving about an inch from the edge. After a minute or two while the egg is still soft, fold over the edges. We have several different styles in our home, pick your favorite:
- fold the egg in half to make a half circle.
- fold the egg in thirds over the filling to make a long tube.
- fold the egg just more than a quarter on four sides so the final shape is a square pocket or envelope with the filling inside.

