---
title: "Creating a Svelte component"
date: 2021-08-15T23:22:53-07:00
summary: "A quick introduction to Svelte and how I learned to get oriented building with new tools."
---

Fun fact: `<audio>` element does not support closed captioning natively. To quote from [MDN Web Docs]((https://developer.mozilla.org/en-US/docs/Web/HTML/Element/audio#accessibility_concerns)):

> The `<audio>` element doesn't directly support WebVTT. You will have to find a library or framework that provides the capability for you, or write the code to display captions yourself. One option is to play your audio using a `<video>` element, which does support WebVTT.

The goal here, though, was a readable transcript, something you might pair with a podcast or interview to let the listener to jump to parts they want to hear. To build this custom component, I decided to try Svelte. As I learned a new framework, for the first time I drafted a checklist of fundamentals to look for when evaluating and learning a new tool. By building a couple components to start the audio player, I'll show those fundamental at work in Svelte.


## What is Svelte

Svelte is a compiler, it processes a special `.svelte` file format into JavaScript and CSS. The `.svelte` file itself is simple: `<script>` , `<style>`, and template markup, all together in one file. You can build stand-alone components (like the audio transcript player built here) or single-page applications. The Svelte project values being small and light: no virtual DOM, building on native web APIs and concepts. I find this approach refreshing, it was a delight to build with.


## Identifying basics: inputs, outputs, loops and conditionals

To start, I used the [quickstart guide](https://svelte.dev/blog/the-easiest-way-to-get-started) from the official Svelte documentation and created a new project using the `degit` option.

Learning _how_ to navigate my way through a new framework or language is a developing skill. There are common fundamentals across any new language, basics you need to build anything. The following list is tailored to front-end development (I'd be curious what this list looks like for other specialities). Here's the start to mine:

- variable declaration and assignment
- inputs (properties and attributes) and outputs (events)
- loops
- conditionals
- async*
- state or storage*

* Though important and worth including, `async` and `state or storage` go beyond what will be covered in this example.


### inputs (`export let`)

Let's start by giving our audio some media. Browsers have varying support for media file formats and codecs. For improved flexibility and cross-browser support, we'll pass in an array property to create `<source>` tags.

```html
<!-- index.html -->
<div id="demo"></div>
<script type="module">
	import { AudioTranscript } from './build/interviews/bundle.js';
	const app = new AudioTranscript({
		target: document.getElementById('demo'),
		props: {
			sources: [
				{
					src: '/demo/how-to-eat-eggs.webm',
					type: 'audio/webm',
				},
				{
					src: '/demo/how-to-eat-eggs.mp4',
					type: 'audio/mp4',
				},
			],
		}
	});
</script>
```

```svelte
<!-- App.svelte -->
<script>
	export let sources = [];
</script>

<audio controls>
	{#each sources as source}
		<source src={source.src} type={source.type} />
	{/each}
</audio>

```

Inputs are as simple as the `export` keyword on the variable declaration. We've also initialized our property with an empty array: `export let sources = []`.


### loops (`#each`)

Already we've seen loops: `{#each}`. The loop ends with `{/each}`. Let's tidy a little with a [destructuring assignment](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Destructuring_assignment):

```svelte
<audio controls>
	{#each sources as { src, type }}
		<source {src} {type} />
	{/each}
</audio>
```


### conditionals (`#if` and `:else`)

Next, let's add a closed caption track. Though the `<audio>` element cannot _render_ WebVTT format natively, as an `HTMLMediaElement` it share an API for processing text cues. This makes it _much_ simpler to parse text, time, and determine active cues (yes, there can be more than one active!).

```svelte
<script>
	export let sources = [];
	export let transcript;
</script>

<audio controls>
	{#each sources as source}
		<source src={source.src} type={source.type} />
	{/each}
	{#if transcript}
		<track default kind="captions" src={transcript.src} srclang={transcript.lang}>
	{:else}
		<p>Required transcript is missing.</p>
	{/if}
</audio>
```

The `transcript` property isn't initialized since there's not a meaningful default. Instead, an `{:else}` clause adds a default element in case the required property is missing. The text block won't render unless no audio source is found--oof, but it's a demonstration.


### outputs (`dispatch`)

Finally, we'll _"jump forward"_ a bit and create a new component to render the transcript cues. Each cue should display its text and give the listener a way to jump the audio to that cue's start time.

First, let's create `<Transcript>`:

```svelte
<!-- Transcript.svelte -->
<script>
	export let cues = [];
</script>

{#each cues as cue (cue.id)}
	<p>{cue.text}</p>
{/each}
```

Next, how do we link selecting a cue to updating the audio current time? One option is attribute binding. We could set `bind:currentTime` on both the `<audio>` and `<Transcript>` elements and let `<Transcript>` set update the time directly. For this exercise, though, what if we were to use events? How would we emit an event from `<Transcript>` up to the audio element with the time it should jump to?

```svelte
<!-- Transcript.svelte -->
<script>
	import { createEventDispatcher } from 'svelte';
	export let cues = [];

	const dispatch = createEventDispatcher();
</script>

{#each cues as cue (cue.id)}
	<p>
		<button on:click={() => dispatch('jumpto', cue.startTime)}>
			{cue.startTime}
		</button>
		{cue.text}
	</p>
{/each}
```
```svelte
<!-- App.svelte -->
<script>
	export let sources = [];

	let cues = [];
	let currentTime = 0;

	function loadCues(loadEvent) {
		cues = loadEvent.target.track.cues;
	}
	function jumpAudio(jumpEvent) {
		currentTime = jumpEvent.details;
	}
</script>

<audio controls bind:currentTime>
	{#each sources as { src, type }}
		<source {src} {type} />
	{/each}
	{#if transcript}
		<track default kind="captions" src={transcript.src} srclang={transcript.lang} on:load|once={loadCues}>
	{:else}
		<p>Required transcript is missing.</p>
	{/if}
</audio>
<Transcript {cues} on:jumpto={jumpAudio}/>
```

To notify components outside `<Transcript>` when the listener picks a cue, we start by creating a dispatcher. When the cue button is clicked, we dispatch a `jumpto` event with the cue start time. To listen to the event, the `<App>` component binds a listener: `on:jumpto`. Our data, here the start time, is found on `event.details`. The audio `currentTime` is set to the cue start time from the event and the audio element will jump the recording to the appropriate cue.


### Summary

In summary, Svelte is a light front-end framework that worked a charm for building an audio payer. With any framework or language, there is a short list of core concepts we need to learn both to start building and to discover the character of the tool we're working with. A few are:

- inputs (`export let sources = []`, `export let transcript`)
- loops (`{#each sources as { src, type }}...{/each}`, `{#each cues as cue}...{/each}`)
- conditionals (`{#if transcript}...{:else}...{/if}`)

We used these to build a couple components that play audio and display the closed caption tracks.
