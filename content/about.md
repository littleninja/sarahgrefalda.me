---
title: "About"
description: "A quick biography about Sarah Grefalda"
type: page
menu: main
---

Hello! I'm Sarah Grefalda. I build software professionally and specialize in web development. Topics that pique my curiosity: accessible design, semantic HTML, new features of CSS, learning SEO, building and scaling apps reliably, and better ways of working together to build software esp. remote work.

I love gardening, be it flowers or vegetables.

I enjoy riding my bike for family rides or commuting.
