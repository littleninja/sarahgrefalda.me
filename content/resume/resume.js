(function() {
  // old school :/
  const lightInput = document.getElementById('light');
  const darkInput = document.getElementById('dark');
  const colorSchemeQueryList = window.matchMedia('(prefers-color-scheme: light) or (prefers-color-scheme: no-preference)');

  const handleInput = (event) => {
    document.documentElement.classList.remove('light', 'dark'); // reset
    document.documentElement.classList.add(event.target.value);
  }

  const syncInput = (queryList) => {
    document.documentElement.classList.remove('light', 'dark'); // reset
    lightInput.checked = queryList.matches;
    darkInput.checked = !queryList.matches;
  }

  // handle input events
  lightInput.addEventListener('input', handleInput);
  darkInput.addEventListener('input', handleInput);

  // handle media query events
  colorSchemeQueryList.addListener(syncInput);

  // set initial state
  syncInput(colorSchemeQueryList);
})();