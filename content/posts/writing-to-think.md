---
title: "Writing to think"
description: "A reflection on using writing to think through problems"
date: 2020-07-28T22:54:45-07:00
categories: ["software development", "personal development"]
tags: ["tools","writing","notes"]
draft: true
---

For a time as a software developer, I worked in a ticket factory. If this isn't familiar to you, that's a good thing. It was hell to me. Much like watching the tip of a match, there's a brief excitement as the flame climbs until quietly, painfully, all that remains is a stick of ash. That tiny joy might be hero syndrome. What do you demo every two weeks in this mode, how do you demonstrate value? Do you toute the number of tickets you crushed? Add some color with a customer gratitude quote? Eventually it occurs to you that you're climbing a sand dune with no shortage of sand. It's a sad existance.

I neglected my growth and didn't develop good habits during that time. Then I requested to join a new team. They had a crisp framework for their workflow and clear priorities and product goals. But me? My mind was a mess. Often I felt like I was frantically walking circles in a fog.

Now, before I go further, your teammates are there to support you, the good ones anyway and I assume that's what you have. But there's a discipline and diligence in vetting your issues first. Perserverence begets grit so when I'd sigh and sent a message to a colleague to say I was stuck, I might have been doing circles but I needed to grow. I could tell it annoyed them, bringing my garbage to their doorstep.

I took some advice and started writing.

## Writing to debug

In hindsight, I ran circles around a problem for a couple reasons. First, I wasn't listening to my environment, I was jumping at assumptions ([another post on the topic of "seeing"](./learning-to-see)). Second, I didn't remember what I tried or why I tried it.

I began this new writing process simply, like a direct message chat with myself.

> What's the problem?
>> I pulled latest (`git pull`), ran `npm install`, ran tests and it errs: "Cannot find babel/broken-thing module"
>
> What are you going to try? Any assumptions you need to check?
>> - [/] `npm ci`
>>   A: Fails with same error
>> - [/] did it fail in our pipeline?
>>   A: Didn't fail pipeline
>
> How could we get more info?
>> `npm ls babel/broken-thing`
>> It's a dependency of `babel/prepare-stuff`
>> `npm show babel/prepare-stuff`
>> Ours: 1.1.0, latest: 1.3.2
>
> What if we upgrade `babel/prepare-stuff` problem module?
>> A: Yes! upgrading fixes the error.

This felt familiar, like being a rubber ducky... for myself. 🦆 As a bonus, maybe counter-intuitively, _it's making me a better rubber duck for others_. It's okay to say "I don't know" and it's okay to ask for help. Writing my steps and my thoughts has allowed me to see my own errors, to practice, and improve.

## Writing to vent

Some days, shit happens. Some days it's a meeting without no clear purpose, a hurtful comment. If it happens early in the day, emotions may burn hot but I can't let it take out the rest of my day. I had someone to confide to but we couldn't meet for a couple hours.

One time I turned to writing to work things out. At first it was a few notes, mostly how this person's comment had hurt me. Then other things unrelated floated to the surface and I wrote about those too. Words trickled at first then poured into paragraph after paragraph. I didn't have to pull punches--this was the letter I'd never send, my eyes only. I let the hurt flow until it was quiet again.

By our meeting, I didn't need to share any of what I'd wrote. _I processed, I healed._ There's an saying by Mark Twain:

> "Anger is an acid that can do more harm to the vessel in which it is stored than to anything on which it is poured."

Writing can empty that vessel.

## Technique

I hope you weren't looking for a silver bullet here, I'm still experimenting with technique. Some examples are the bullet journal method, Zettelkasten method. Some stand by pen and paper, others digital form. All of these--they're all tools. Use what serves you.
