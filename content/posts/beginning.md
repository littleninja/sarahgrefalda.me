---
title: "Beginning"
description: "Not everyone in software engineering brings a computer science degree, and many journeys wander a bit."
categories: ["personal development"]
tags: []
date: 2020-11-29T20:36:21-08:00
draft: true
---

It's a common opener: "how did you become a software engineer?" Maybe you're here because you've asked me the question and I pointed you here, or maybe you're asking yourself the same. I hope that my candor helps you assess your qualities and the requirements of the job to make the decision simpler--and I hope it's a good fit. See you out there!

---

If I tell you how I got my start in software development, how far back do I go? Diapers is probably too far, quite probably irrelevant. But I can tell you that in school, I loved math and science--not so much computers. Adults put too much weight on high school students, architecting their life at the young age of seventeen or eighteen. I picked my college, I studied civil engineering. I certified into my major then hit The Wall. I wrote it off as wanting more art or creativity and switched to landscape architecture, but not long after felt lost and took time off. To make a living, I accepted a job at a flower shop working customer service. Yes, I do miss "the office": peonies in early summer, dahlias and sunflowers in autumn, cedar and pine at Christmas time. You can get into a meditative state arranging rose arrangements and in those moments I thought to the future. Did I want to become a shop manager, wedding designer, or owner? Would it support the family I wanted to start? What about standing on my feet all day or the possibility of carpal tunnel? It was time to go back to school and find a job that left me energy to study.

![a pitcher vase of pink and purple stock flowers](./beginning/annie-spratt-zWCUF5pTIM4-unsplash.jpg)

_Photo by [Annie Spratt](https://unsplash.com/@anniespratt?utm_source=unsplash&amp;utm_medium=referral&amp;utm_content=creditCopyText) on [Unsplash](https://unsplash.com/s/photos/stock-flower?utm_source=unsplash&amp;utm_medium=referral&amp;utm_content=creditCopyText)_

I went back to school online and took a job in tech support for an education company. That's where I had my first taste of debugging--and I liked it, a lot. Many calls followed a similar path ("clear cache and cookies" anyone?), and I loved being looked to for help with the complex, technical cases. Reporting bugs and listening to our teachers and students were great for domain knowledge and I had a knack for asking questions. And I was lucky: a few folks in the technology group kept an eye on me. I joined our quality assurance team and kept learning: I wrote test plans, got to know HTML, CSS, and JavaScript better. I learned a little bit of SQL and set theory, too.

There are special memories in the learning journey, like understanding variables. Math uses variables and I loved math, so why did they sometimes work differently in software? I practiced on Codecademy, the side-by-side lesson with practice was helpful to see how this all works.

```javascript
var x = 1;
var y = x + 1;
console.log(y); // 1 + 1 = 2, yay!

var z = 1;
for (var i = 0; i < 10; i++) {
  z = z + i; // um, how is z equal to z + 1??
}
```

Around this time, tech elevated cross-functional teams and we were going to use XP (Extreme Programming). I upleveled my testing game from spreadsheets to automated test suites, and I learned other roles, too. I helped with our build automation and learned design patterns asking questions when I paired with engineers. That said, these were still my early days. Scripting? Getting better. Software? Not yet.

My manager was one of my first mentors. He had a similar background, so we worked together to continue applying what I knew and start learning computer science fundamentals. It wasn't magical: Java and its compiler still make me sour, it was the language of choice for the Princeton algorithms course (I got frustrated and dropped). I completed a different algorithms course taught in Python and I _kept_ learning: basics of machine learning (Coursera, taught by Andrew Ng, highly recommend it) and basics of design.

> In case it helps others starting their journey, here are the resources that helped me. As I'm writing this list, I have to remind myself that I had ups and downs, I completed these over the course of _years_.
> - [The Pragmatic Programmer, First Edition](https://www.oreilly.com/library/view/the-pragmatic-programmer/020161622X/). Read this as a quality assurance engineer, helped me understand what developers were talking about: interfaces and decoupled design, how to test well. I need to revisit this one.
> - [JavaScript: Understanding the Weird Parts by Anthony Alicea](https://www.udemy.com/course/understand-javascript/). For learning JavaScript more deeply and discovering some of its wonderfully bizarre quirks. The exercises helped concepts take root.
> - [JavaScript: The Good Parts by Douglas Crockford](https://www.oreilly.com/library/view/javascript-the-good/9780596517748/). A dense read that helped me dive deep on JavaScript.
> - [Introduction to Computer Science and Programming Using Python](https://courses.edx.org/courses/course-v1:MITx+6.00.1x_7+3T2015/course/). Python was an approachable new language and made it easier to learn the computer science material.
> - [Machine Learning by Andrew Ng](https://www.coursera.org/learn/machine-learning/home/welcome). Fantastic instructor! Machine learning might seem like a leap from web development, but I was hungry to learn more about linear algebra after completing a math interactives project.

I decided to accept a full-stack engineer position in the airline industry, but a large part of the job is e-commerce. The  "full-stack" part pushed me into uncomfortable new waters: what's DNS and why did it just cost us a 1-hour outage? Load balancers, certificates: these were new and part of the full-stack gig. There are new things I'm aware of but still need to learn more: privacy and PII and PCI compliance, SEO, security and pen-testing, monitoring and alerts or site reliability engineering, infrastructure as code or devops. Again, I have much to learn (depth) in each of these categories.

> Here's a list of things where I still don't have much depth of knowledge:
> - Unix shell
> - mobile native development
> - privacy, PII and PCI compliance
> - SEO
> - security and pen-testing
> - monitoring and alerts, or site reliability engineering
> - infrastructure as code or devops
> - networks

But I'm realizing maybe I don't want to be solving e-commerce problems. Maybe I can find somewhere where misfits like me fit in. I've learned that the people you surround yourself with and autonomy at work can make or break my wellbeing. I understand front-end in some depth, I grok full-stack--that's what I bring to the table. I'd love to pair with someone, some people, who bring something new for me to learn. More write-ups, more scientific process. Maybe it'll be machine learning or biology, who knows? I love learning, and I'm eager to see what's next.
