---
title: "Pride, prejudice, and hats"
description: "A reflection on a difficult experience advocating for inclusive design in Stack Overflow's Meta community"
categories: ["personal development"]
tags: ["stack overflow meta","diversity","inclusion"]
date: 2021-01-04T16:00:00-08:00
draft: true
---

Each year brings a new Winter Bash to Stack Overflow and with it a new round of clever hats. Hats rise to all levels of whimsical and creative: a surgical mask for the pandemic this year, a Tik Tok frame, a hat made of fruit, a barfing-rainbow Snapshat mask.

This year I was surprised with a secret hat: "Quarantine Hair", a shaggy mess of black hair with a little bird's nest on it. It was delightful! Then something weird happened. My avatar was a photo with short hair but the hat-hair was still shorter. If the joke was uncut hair, why were my kempt locks _longer_? I reassessed the hat, applied a mental hair cut to it, and had an ugly thought. _Did they just give me a man's haircut?_

![An overgrown short haircut with a little bird's nest](quarantine-hair.svg)

It's hard to explain how this feels, someone imposing a harmful stereotype on you and the subtext saying you don't belong. I intentionally keep my Stack Overflow profile neutral to prevent discrimination, yet users address me with "he/him" pronouns or expressed gratitude with a "thank you, good sir!". Why assume I'm a man?

And I confess, I assumed the worst. Was the designer a man? Did they model the hair after a stereotype? I had a question... so I went to go ask on Meta.

The original title of the question tried to assume good faith: "Was the 'Quarantine Hat' designed to be gender-inclusive?". Later I would form it into an accusation before reverting it back again: "Why was the 'Quarantine Hat' not designed to be gender inclusive?". Not my best moment.

[I posted](https://meta.stackoverflow.com/questions/404218/was-quarantine-hair-hat-designed-to-be-gender-inclusive). One downvote. Two. Three. Did downvotes mean "no, this hat _was_ gender inclusive" or "I _don't care_ if the hat wasn't gender inclusive"?

After a day or so I did want to come back and find an answer. _Was it_ gender-inclusive? Well, what would a gender-inclusive haircut look like? In hindsight, I could have researched that exact question and presented this in my original post. As it was, I looked for prior art in the history of emojis and learned of Paul D. Hunt who in 2017 designed and proposed gender-inclusive persons into the Unicode emoji character set.

Hunt studied what features identify a face as masculine or feminine. Curious to see the iterative process, I found this image [1]:

![Hunt's design iteration on a gender-inclusive person emoji](paul-hunt-emoji-iteration.webp)

The tousled, short hair looked remarkably close to the quarantine hair design! A troubling new thought emerged: what if it was _me_ who had misread the hair as masculine? Surprisingly, Hunt's research had a possible explanation for that, too [2]:

> As Hunt learned from conducting online surveys, he says, "there's a tendency in our culture to view things as masculine by default." On the other hand, emoji explicitly meant to represent women and girls are overly gendered—doe-eyed, lipsticked, and hairstyled to the point of reading as feminine caricatures.

I shared an answer that explained why the design _might indeed_ be inclusive. My answer received downvotes, too. I still don't know the answer, if it was inclusive. As another answer noted: "the jury's still out". I hope I didn't wrongly accuse the designer. An early comment reminded us to "assume good intent" and it took distance to realize I could have approached my question better.

Finally, I bristled at the replies:

- "if we over-analyze each hat, Winter Bash becomes a lot less fun, if not downright impossible"
- "if you don't like the way the hat fits, you don't have to wear it"
- "there will ALWAYS be someone that can feel [offended]"
- "it's a hat, not an exact representation of real-life"

It's quite normal to defend, well, the norm. The replies hinted at zapping the fun for the majority, I could opt out, we could never achieve perfection. Excuses. My goal is allowing myself to belong in the software community--because we code, not because we're men. Don't let 'perfect' get in the way of making progress.

At the end of it all, looking at my unpopular question and answer, I felt tempered pride. Not for being unpopular, no. I put in the work answering a difficult question about gender inclusion. I didn't let the fear of criticism, not getting it right first try, or label of "that annoying person" stop me from engaging. Briefly, I understood the art of not giving a fuck. I care very much about inclusion in software engineering. Do I care if I win the hearts, minds, and votes of Meta? Not really. I measured my success here by my personal growth. That indeed was worth the journey.


## Resources

More about Paul Hunt and emojis:

- [1] ["Can Emoji Be Gender-Inclusive? Paul Hunt Has Designed His Way to an Answer." by Christina Cauterucci, published by Slate on June 30, 2017.](https://slate.com/human-interest/2017/06/paul-hunt-made-gender-inclusive-emoji-for-non-binary-individuals.html)
- [2] ["Designing Genderless Emoji? It Takes More Than Just Losing the Lipstick" by Megan Molteni, published by Wired on June 30, 2017.](https://www.wired.com/story/designing-first-genderless-emoji/)
- ["How an emoji goes from pitch to product" by Megan Farokhmanesh, published by The Verge on December 19, 2016.](https://www.theverge.com/2016/12/19/13927588/emoji-creation-process-paul-hunt-designer-adobe-unicode-interview)

In Hunt's own words:

- ["What is Gender and Why Does it Matter to Emoji?" by Paul D. Hunt, published on Emojipedia on March 20, 2017.](https://blog.emojipedia.org/what-is-gender-and-why-does-it-matter-to-emoji/)
- ["Proposal to enable gender inclusive emoji representation" by Paul D. Hunt, addressed to Unicode Consortium on October 24, 2016.](http://www.unicode.org/L2/L2016/16317-gender-inclusive-emoji.pdf)
