---
title: "Building an Academic Portfolio"
date: 2021-12-26T23:05:30-08:00
draft: false
tags: ["hugo"]
---

Over Thanksgiving, my sister had an idea brewing. "I have enough publications and projects out there, I'd like to put everything together on a website." She's quite tech savvy and talented with design. I encouraged her to go for it and let her know I would be here to support her if she wanted.

A couple weeks later, she texted: could we build this together? Heck yes! We [successfully launched her website](https://www.miriam-hacker.com) just after Christmas.


## The stack

Real quick, to cover the basics: my sister and I started our collaboration with a shared Google Drive, drafting the content structure, and looking at aspirational websites to understand what she envisioned.

The specifics I'll put to a table:

| Technology | Purpose | Rating |
|:-|:-|:-:|
| [Hugo](https://gohugo.io/) | Static site generator (SSG) in Go programming language | 💯 |
| [Wowchemy](https://wowchemy.com/hugo-themes/) | Pre-built theme for Hugo ([Academic Resumé](https://github.com/wowchemy/starter-hugo-academic)) with lots of batteries included | 🎉 |
| [Netlify](https://www.netlify.com/) | Build-deploy cloud platform | 💯 |
| [Git](https://git-scm.com/) | Distributed version control system | 💯 |
| [GitLab](https://about.gitlab.com/) | DevOps platform, hosts Git repository | 💯 |
| [Forestry](https://forestry.io/) | Content management system (CMS), an editor compatible with many popular SSGs (incl Hugo) that sits atop your hosted source code (incl GitLab) | 😕 |
| [Google Domains](https://domains.google.com/registrar/) | Domain registrar, custom domains | 💯 |
| [GoatCounter](https://www.goatcounter.com/) | Analytics for site traffic | ⏲[^1] | 
| [ffmpeg](https://ffmpeg.org/) | A bit of light media file processing | 💯 |

And a hastily drawn diagram shows how the pieces fit together:

![system diagram showing my local Hugo server and git commit setup, my sister editing online with Forestry, and GitLab, Netlify, and Google Domains making the published content available online](./system-diagram.png)


### Decisions and tradeoffs

The action I'm most proud of was _listening_, hearing what was important to her. Two priorities guided our decisions:

1. speed to launch
1. customizing the site design

When speed is critical, **play to your strengths**. I've spent the past year building my personal site with Hugo because it's fantastic for making websites with static content. Hugo was a solid choice.

Though on my personal site I've forked and developed a theme, I did _not_ build a custom theme for my sister. Why? My theme is a work in progress, and one that I've tinkered with to suite my interests, my problems. It is incomplete. Trying to build and release a custom theme while at the same time building the site would have been a **distraction** and would likely have delayed launch.

The academic portfolio is about my sister and her content, so when we searched Hugo themes and found one that specialized in academic content, it ended up being a great fit! Because I don't live in the academic world, there were all sorts of features she recognized and immediately found useful that I wouldn't have thought of, for example iconography for links to Google Scholar or a `url_doi` field. Though I didn't build a custom theme, **the time I spent tinkering educated me on Hugo's core concepts**: lookup order, front matter, configuration, convention and structure. The theme publisher had great documentation, and my experience gave me context to search more effectively and ask better questions.

I had a third priority: ease of content editing for my sister, and I missed the mark. Forestry was a gamble, I knew better: **gamble on new services and technology on your own time, not with clients**. Being well-equipped to customize and modify the site was a key reason for choosing Hugo over Wordpress. Forestry and platforms like it make it possible for non-coders to manage and add content to their sites without coding, and it supports several popular generators. However, the following gaps were a problem:

1. **No preview server, outdated versions of Hugo** : On time of writing, the latest Hugo version is 0.91. The pre-built theme used version 0.84, the latest available by Forestry 0.81. This meant my sister could see edits in a WYSIWYG editor but couldn't preview them as part of the site before publish.
1. **Limited abstraction of Hugo's full capabilities** : Hugo can support either a single post file (`this-post-is-great.md`) or a directory which is especially useful for organizing related media (`this-post-is-useful/index.md`). Forestry could interpret either structure but my sister couldn't upload a featured image into a directory. To flip the problem, the pre-built theme might have used front matter instead of convention to support a featured image just like it does to support alt text and captions. 🤷🏼‍♀️ Oh well, it's still one of a few missing capabilities for managing a Hugo site:
	- can only upload media to one static directory
	- adding a post to a directory, no option to rename the file to `index.md`
	- cannot edit Hugo configuration or theme configuration files (maybe a limitation of the outdated Hugo version?)
1. **No viewing or managing version history from the editor** : It comforted me knowing "it's all Git". Nearly any mistake could be fixed with a quick `git revert`--but not in the editor. There's a Site Activity icon by the logo but no way to see commit hashes to a file or revert to a change like you might in GitHub or GitLab.
1. **So many commits** : Each save is a commit, and more commits is more build minutes. Over the week I jumped over 100 minutes of 300 minutes available (normally I average 5-10 minutes per month).

	Sidenote: I think what this made me realize is I want something like a branch feature so not _every_ change needs to be draft-or-live: you can save changes to a "release", and publishing your changes is like merging your branch into the main development branch. I'm sure this is challenging (rebasing branches, for one) but it's an idea.


### Summary

- Listening: a winning strategy.
- Playing to your strengths is good for speed. There's nuance but when what you know solves the problem, go for it!
- My personal projects--using Hugo, developing my own theme, integrating components from another site (CORS), using Git--made it easier to build, customize, and debug. Win! 🎉
- DNS changes can take a long time, up to 24 hours or longer. Prepare accordingly.


## What I'm curious about next

- What is Netlify or Git LFS (large file system), how does it work?
- Check out Netlify CMS--will it support directory content with media?
- Build my own theme!
- Check out 11ty (Eleventy)

[^1]: I use GoatCounter for basic analytics. Once the site has traffic, we'll assess if it meets her needs.
