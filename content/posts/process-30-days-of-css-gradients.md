---
title: "Process: The Mess Behind the 30 Days of CSS Gradients Project"
date: 2021-11-29T21:13:41-08:00
draft: true
---

## Things I learned

- I started using the [Material Design color palettes](https://material.io/design/color/the-color-system.html#tools-for-picking-colors)[^1], I thought the constraint and pre-picked color would make it easier to focus on using color, not picking it. Maybe it did. Eventually I noticed the colors weren't pleasing: not enough saturation, gaps in hues that I wanted. It took until the last week but I finally gave up the constraints of Material colors and swiveled to [Paletton](https://paletton.com). Continuing on the topic of saturation, I also observed a growing preference and joy using saturated colors.

- There are weird thing you can't do, or that I haven't figured out how to do. Like repeating a line segment with transparency. Or rotating an ellipse, or repeating a background image around an axis (asking too much with that last one).

- The journey from crap to exploration and wonder. This is also why about mid-way through I decided to flip the order from newest to oldest. The first week produced crap, and I learned to finish a piece and move on. What's delightful is seeing the progression as I moved through the second and third weeks, you can see the growth and that is so exciting! 

- It was fun allowing recurring themes, to find a sticky idea and build something for the day and feel okay coming back to it again after a few days or a week. This also made growth more visible. I worked so hard on day 3 star design, all the math, but composition and color were... not good. I come back to it on day 22 when I relax on the math and try to recreate this Day 8 I'm toying with an Escher cube illusion, revisited and explored starting on day 15. There's a bit of item composition, tessalation, and what might fly for a background on a hero component.

- Being introduced to myself. I'm a people pleaser, I strain myself trying to understand what pleases people and what they want of me. It's deceptively inefficient: not trying anything until you're asked to do it, then working furiously to figure it out. And it's quite unpleasant. This project was the opposite, like a 30 blank pages to fill with whatever pleased me so long as I put _something_ on each page. Having tried, in my gut I _know_ which ideas hold more possibility for me, that I'll want to explore more--and which ideas are good enough and set down.


---

[^1]: OMG, **2014** Material Design color palettes?? They continue to stand tall building cohesive palettes for applications but it took a while for me to see I was using a beautiful hammer on a screw.
