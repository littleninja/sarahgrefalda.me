---
title: "Keep Making"
description: "Keep going,keep building on your successes and learning from failure"
categories: ["personal development"]
tags: ["grit"]
date: 2021-01-12T21:40:16-08:00
draft: true
---

🥚

You sign up for Design 101. You're curious, you want pages with whitespace that lights up the room and imagery fit to frame. Some of the homework assignments introduce you to a graphic editor. Curious about the rich, colorful illustration all the cool designers are publishing these days, you make one.

You illustrate an egg.

🔐

"Why don't you write something? Make a blog?" Why not? I'm not qualified, you reply silently. But the idea sticks. Why not write something? How _does_ one host a blog? The idea is sticky, it lurks.

You write in your markdown diary one night. Hell no this isn't going on the internet.

📻

Taking classes and reading technical books competes for attention from your (very persistant) kids. They plead, sometimes climb. Make something with them. Create together.

You record an interview of your kids explaining how to eat eggs.

---

Months go by, years. You squeeze time to study between reading books to your kids and making play dough and spending time in the garden. A couple private repos sit around holding solved toy algorithms, little puzzles you've solved. Your kids are thriving but the code commits are specks on your GitHub profile. Reality darkens as you more often get asked to present a portfolio, your side projects. Wherever those are hiding.

Someone once told you makers create, they build a stockpile of dry tinder material ready to take the spark of inspiration. These creations, they invite new branches to explore. You have a blog built with Hugo now, you're learning GoLang. You want multiple file formats to use with HTML5 audio so you finally have an excuse to play with ffmpeg. And then there's writing. You play with prose, listen to audio books on the topic of writing, chemistry, and whatever becomes available next from your well-stocked Holds list.

Keep going.
