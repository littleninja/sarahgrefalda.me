---
title: "Fixing My NodeJS Install"
date: 2021-12-29T22:04:07-08:00
draft: false
tags: ["nodejs", "macos"]
---

For a long time, I've had an old version running on my personal laptop (macOS Big Sur 11.16.1) and until now I had been trying to ignore it:

```shell
node --version
# 12.3.1
```

Seeing `nvm` was already installed (had I used brew?), I thought, "oh yeah, I already tried to fix this months ago". The result then after installing `nvm` and LTS version was still the same old version. Fed up with guessing and trying to patch things up, my path forward was to:

1. remove all files installed by NodeJS, including the previous `nvm` install;
1. install and uninstall with homebrew to help with the cleanup; and
1. install and use `nvm` to install NodeJS.

> Note: **Remember to close and open a new terminal window between some of these steps.** Many executables, NodeJS included, will update to PATH and other environment variables, and your current terminal session won't have those updates. A common way to fix that is to close the terminal window and open a new one.


## Finding all the dangly directories

To start, I did a web search for "brew uninstall node" to find [this StackOverflow question](https://stackoverflow.com/questions/11177954/how-do-i-completely-uninstall-node-js-and-reinstall-from-beginning-mac-os-x). Scanning the answers and comments over the years, NodeJS and some global modules install things, well... everywhere.

```
/usr/local
|_ bin/node
|_ bin/npm
|_ include/node*
|_ lib/dtrace/node.d
|_ lib/node*
|_ share/man/man1/node.1

$HOME
|_ .node*
|_ .npm*
|_ .nvm
```

Through trial and error, I kept poking (`ls -al`), removing (`rm -rf`), then closing the terminal and checking if it was finally gone (`node --version`).

> Note: `which node` is super useful if you want to see where the node executable is hiding!


## Uninstalling the node cask (brew)

I admit the following was a little bit superstitous. I _probably_ installed NodeJS from the website but I suspected I had also tried `brew install node`. Also, there were probably still PATH variables or symbolic links still hanging around, and I wasn't sure how to manually sniff them all out. To cover my bases, I uninstalled the node cask and did some extra cleanup--a modification on an [answer on StackOverflow](
https://stackoverflow.com/a/27008484/3294944):

```shell
brew update
brew uninstall node

# check for warnings
brew doctor

# remove old lockfiles, old downloads, old versions
brew cleanup node
# remove only symlinks and directories for prefix
brew cleanup node --prune-prefix
```

As an extra precaution, I also removed the nvm cask:

```shell
brew uninstall nvm
brew cleanup nvm
```

To verify both were removed, I closed and opened a new terminal to check:

```shell
node --version
# node: command not found

nvm --version
# nvm: command not found
```

## Doing it the right way: nvm

With NodeJS uninstalled, going forward `nvm` would manage NodeJS versions. The [official instructions for install and update on GitHub](https://github.com/nvm-sh/nvm#installing-and-updating) were easy to follow:

```shell
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.1/install.sh | bash
```

Next, I closed and opened a new terminal and verified the install:

```shell
command -v nvm
# nvm
````

Finally, I installed LTS and used it:

```shell
nvm install --lts
nvm use --lts

node --version
# v16.13.1
```

This whole ordeal was messy. Have you had troubles trying to update NodeJS? Did you solve it and update your version? Please send me a message on Twitter, I'd like to hear about your experience!

## PS. A maybe-outdated script still worth mentioning

In [an answer to an earlier StackOverflow question](https://stackoverflow.com/a/11178106/3294944), @brock commented to share [a script in a GitHub Gist](https://gist.github.com/brock/5b1b70590e1171c4ab54). The script was written years ago and though recent comments suggest it's still in use, the script installs **old versions of NodeJS (0.10) and `nvm` (0.0.4, I think)**. The effort and value automating all these steps is commendable, and if the author is open to it, an update would be a great way to give back to the open source community!

