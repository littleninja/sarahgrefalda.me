---
title: "Asking Good Technical Questions"
description: ""
categories: ["software development", "personal development"]
tags: ["questions","searching"]
date: 2020-12-08T21:04:26-08:00
draft: true
---

Learning to code has so many thrilling moments. If it's nerdy to feel elation when a calculation happens or text and shapes appear on a screen, I don't wanna be cool.

I believe in felt.

But steele yourself, there are so many disappointments. Things don't move, they don't show up, don't work, errors in the console, or worse--no errors in the console. And it's common to feel so close, desparate for a little help. What to do? You can post a question to StackOverflow. As you connect with peers and find mentors, you can send a chat. Or casually drop a comment at work, us engineers **love** a juicy problem.

But here's the thing: debugging is a skill you can learn and it'll keep you moving on your coding journey.

## Document your (de)bug

Open a file and start explaining. Easily skipped but most important, start by explaining the problem. What was it you were trying to do and where did things get stuck? What's the exact error? What did you try, what did you check to make sure you're doing this right? Often the best clues and sometimes the answer are sitting there, waiting for you to put a few puzzles pieces together. If you have to dive deep, more than a few failed attempts at solving your problem, this technique is like breadcrumbs in a forest. It's not intended to bring you home but if you pass a trail of crumbs, beware.

Here you're explaining things to yourself. What's fantastic is you're preparing just enough material to help someone be useful to you, and you're practicing the skills that will elevate you to the next level to _be_ that senior engineer or mentor. Start by explaining what you were trying to code, I love using prose for this. Next, what went wrong, **exactly**? Don't use "hand-waving" or generalities here. Having a vague idea is okay but walk it to a more precise assertion or an experiment to disprove your assumptions. (The 5-why's exercise can be helpful getting you to dive in if you feel stuck on generalities. Another technique is an experiment: given how X should work, what's a simpler way to prove it? Even more difficult for me: what would disprove my assumption here? For example, I assume the file was cached, so inspect network requests and check for cache control headers. Cached on the server? How long and which requests, how might I prove a change isn't returned because it was cached?) Specificity and precision are important. Next, check the basics and your assumptions. What are the necessary pieces for your code to work, and are you sure your inputs and semantics are correct (or what you expected)? Last, as you start to make a change at a time, log what you're doing and why you think it will make your code works. When it does, yay! You can choose to save this log for later. (This is why I love searchable markdown files, I can search for a term or parts of an error message and recall an answer.) If you don't, it protects you from hitting a wall when you try something again and wonder why your code still doesn't work.

Thinking about it a different way, what if you opened a StackOverflow question? It starts with a succinct one-line question (could future-you search for this?). Next, if you can, create a minimal complete example that shows the problem you're having. Sometimes our code lives in closed repositories but even open source problems benefit from the _focus_ and _repeatability_ of this exercise. And if you've found a bug, you're a step ahead to report it! Last, sharing your research--links, troubleshooting attempts--means people trying to help you don't spin circles on the easy stuff you can try on your own.

## Spectrum: asking too soon, asking too late

Remember: sometimes we ask too early (stupid missing semicolons). But it's possible to wait too long. If you've started putting notes or pieces together and you're still stuck, it's okay to reach out. The notes serve you, and their a head start for getting help from someone who might have a fresh perspective or know a little more.

## Encouraging good questions

## Notes

- how to explain the Socratic method, what is it? what's its origin?
