---
title: "Using SVG Sprites"
description: "Vector graphics! Now in highly reusable sprite format!"
categories: ["software development"]
tags: ["svg"]
date: 2020-10-28T11:47:04-07:00
draft: false
---

_Summary: SVGs are excellent for icons, you can scale and restyle them with CSS. SVG symbols can be bundled together and referenced like a template--from an external file even! But proceed with caution:_
- CORS
- Internet Explorer 11
- print view 🖨

---

It wasn't a complicated task, putting three 32x32 icons on the page. A few `<img>` tags would have done the trick but _noooo_. Another image format, SVG, let's you do so much more! For starters I wanted to try customizing fill and stroke colors with CSS. Libraries like `d3.js` use SVG to draw rich charts--and animate them, too! The vector graphic "VG" part of the name gives the image format its scaling super powers. When I learned about SVGs and all they were capable of, Baader-Meinhof phenomenon took full effect and it has been fascinating, all the places it shows up on the web!

There are a few ways I could put an SVG into a page:

1. I could put the SVG markup directly into the content (embedded).
1. I could use Javascript to fetch and inject the SVG into the page (injected).
1. I could apply a `<use>` element to copy a symbol from elsewhere, even another file (external URI or sprite).

Note that I'm not including other possibilities like an `<img src="">` or `background-url` style. When loaded this way, the SVG is interpreted like a rasterized image. Customization and scaling benefits are lost.

The penalty for embedding SVG is the same for embedding any other type of static content into dynamic responses: heavier payload and longer response time. SVGs can be smaller than other image formats, but duplicate icons in a page can add up and it misses the opportunity to be cache-loaded. With injection, loading a large number of images on page load with Javascript can flood the browser with network calls. Here bundling the images into a single file (sprite) has clear advantages. So what are the drawbacks?

## CORS or Cross-Origin Request Security

Simply put, SVG doesn't support crossorigin requests with `<use>`. It's disappointing: it's common pattern to host static assets from a CDN on a different domain from the dynamic content. I was curious and what I've gathered learning about SVGs is that they're pretty powerful--you can put image and script elements inside them. When our browser wants a potentially risky resource on a differnent origin, we establish trust with CORS headers. It's a handshake of between browser and server, _both sides participate_. The browser asks permission with request headers, the server grants permission with response headers.

What I'm learning is the SVGs spec was written before cross-origin security. Without a security spec for SVGs, browsers don't include the CORS request headers when appropriate. No cross-origin support. 😞 What's more--I commonly use `file:` protocol to play around with my code locally. Because `file:` protocol doesn't use cross-origin permissions, `<use>` external references flat-out won't work. You'll need to host the files locally (I use `http-server` installed as a global npm module) and switch to `http:` or `https:` protocol. Quite a bit of this was a new understanding brought together by the O'Reilly Media link in the resources below, I encourage you to read the source!

## Internet Explorer 11

Nope, no one likes IE. Not even Microsoft. I'll be brief: even if you host your SVG files and pages under the same origin, `<use>` external URI references are unsupported in Internet Explorer. Chris Coyier wrote a couple articles with more depth on using SVGs in Internet Explorer, links are in the resources below.

## Printing 🖨

This is still a mystery, clearly I need to dig into what happens between browser and printer driver to make printing magic! Given the earlier discussion on CORS and `file:` protocol, I suspect similar here but again there's still more digging to do. But use caution and bring a backup plan if your page needs to be printable.

## For the times when you _can_ use an SVG sprite (it's beautiful!)

Creating a SVG sprite file consists of:

- start an XML document
- add a root `<svg>` element
- to the root add a `<defs>` element for your symbol definitions
- add each `<symbol>` element with a unique id with the vector elements inside

Using a symbol is as simple as referencing it by its `id`. In the example below with octocat `#icon-github`, if the symbol definitions are already embedded on the page, I only need to reference by hash. If loading from an external file, the relative or absolute URI comes before the hash.

```html
  <!-- embedded svg example -->
  <svg>
    <use href="#icon-github">
  </svg>

  <!-- external URI example -->
  <svg>
    <use href="sprite.svg#icon-github">
  </svg>
```

Here's an excerpt taken from the end of my experiment:

sprite.svg
```xml
<?xml version="1.0" encoding="UTF-8"?>
<svg xmlns="http://www.w3.org/2000/svg">
  <defs>
    <symbol id="icon-github" viewBox="0 0 16 16">
      <path fill-rule="evenodd" clip-rule="evenodd" d="M8 0C3.58 0 ... 8 0Z"></path>
    </symbol>
    <symbol id="icon-linkedin" viewBox="0 0 16 16" stroke="none" stroke-width="1px" fill="none" fill-rule="evenodd">
      ...
    </symbol>
    <symbol id="icon-gitlab" viewBox="0 0 16 16">
      ...
    </symbol>
  </defs>
</svg>
```

index.html
```html
<body>
  <p>some text before</p>
  <a class="primary-color">
    <svg width="32px" height="32px">
      <use href="sprite.svg#icon-github">
    </svg>
  </a>
  ...
  <p>some text after</p>
</body>
```

![my SVG icons loading from a separate file!](theme-light-working-icons.png)

Thanks so much for reading. I hope you'll be inspired to open your developer tools and look for more SVGs in the wild, and maybe _just maybe_ have some fun using them too. Stay well!

## Resources

- [O'Reilly Media: Understanding CORS and SVG](https://oreillymedia.github.io/Using_SVG/extras/ch10-cors.html)
- [SVG Authoring Guidelines by Jonathan Watts (2005, last updated 2007)](https://jwatt.org/svg/authoring)
- [Icon System with SVG Sprites by Chris Coyier (2016)](https://css-tricks.com/svg-sprites-use-better-icon-fonts/)
- [SVG `use` with External Reference, Take 2  by Chris Coyier (2017)](https://css-tricks.com/svg-use-with-external-reference-take-2/)
