---
title: "Installing FFmpeg"
description: "How to install FFmpeg on macOS"
tags: ["ffmpeg","macos"]
date: 2021-01-14T21:49:15-08:00
draft: false
---

Researching a company in automated speech recognition, I had created a test account on their platform. Wanting to try transcription, I looked through my phone for a sample. That's when I rediscovered a few interviews saved in Voice Memo on my phone. It was nostalgic moment--and the trascriptions were okay. The interviews sparked a project idea and it was the nudge I needed to (finally!) install FFmpeg, an audio and video converter, and convert my first audio files.

## Installing FFmpeg

To begin, I Air Drop'd the interview files from my iPhone to my macOS laptop. The file format was m4a (mpeg-4 audio), I would convert my audio file to WebM and MP4 format for a project on a website. Years ago another engineer had mentioned FFmpeg but I didn't grock the installation steps at the time. Without a good reason to push through, I set it aside. Now was a perfect time to try again!

The install was rough. If you're looking for the final steps, jump ahead to [the quick solution](#the-quick-solution). The maintainers do not produce a DMG installer for Mac and that seems reasonable. There was an option to download the zipped executable built for my system, so after downloading and extracting `ffmpeg`... then what? Fumbling, I double-clicking the `ffmpeg` file and "something happened" but it didn't result in `ffmpeg` being recognized as a command.

This is roughly how my troubleshooting efforts went:

```shell
ffmpeg
# -bash: ffmpeg: command not found

./ffmpeg
# -bash: ~/ffmpeg: Permission denied

# me: dragging ffmpeg into Applications ¯\_(ツ)_/¯
# me: um... where'd it go? oh, found it: /usr/local/bin

/usr/local/bin/ffmpeg
# -bash: /usr/local/bin/ffmpeg: Permission denied

chmod +x /usr/local/bin/ffmpeg
/usr/local/bin/ffmpeg
# -bash: /usr/local/bin/ffmpeg: cannot execute binary file

file /usr/local/bin/ffmpeg
# /usr/local/bin/ffmpeg: MacOS Alias file

rm /usr/local/bin/ffmpeg

# me: re-downloading and extracting the exe again
mv ./ffmpeg /usr/local/bin
chmod +x /usr/local/bin/ffmpeg
file /usr/local/bin/ffmpeg
# /usr/local/bin/ffmpeg: Mach-O 64-bit executable x86_64

ffmpeg
# me: success!
```

### The quick solution

I cleaned up files and retraced my steps. Here's how it went:

Note: There's more than one way to download: by **terminal** or **browser**. Evermeet hosts the ffmpeg executable, see their [download API](https://evermeet.cx/ffmpeg/#api-download). If downloading from a browesr, go to the [ffmpeg.org](https://ffmpeg.org/). Browsers can flag zipped executables as malware, so you may need to override security settings to allow the download.

```shell
# 1. Install the zipped executable, or visit https://ffmpeg.org
curl -JL -o ~/Downloads/ffmpeg.zip https://evermeet.cx/ffmpeg/get/zipcurl -JL -o ~/Downloads/ffmpeg.zip https://evermeet.cx/ffmpeg/get/zip

# 2. Inflate/extract executable file
unzip ~/Downloads/ffmpeg.zip -d ~/Downloads/ffmpeg

# 3. Move file into executables directory
cp ~/Downloads/ffmpeg/ffmpeg /usr/local/bin/ffmpeg

# 4. Allow file to execute
chmod +x /usr/local/bin/ffmpeg

# 5. Quick check
file /usr/local/bin/ffmpeg
# /usr/local/bin/ffmpeg: Mach-O 64-bit executable x86_64

# 6. Ready set go!
ffmpeg
```

In the grand scheme it was a tiny milestone, installing FFmpeg, but heaps of small trials and failure built up to this success!

### The result: converting my first audio files

Converting an `.m4a` file to `.webm` and `.mp4` was as simple as:

```shell
ffmpeg -i ~/path/to/how-to-eat-eggs.m4a \
~/path/to/how-to-eat-eggs.webm \
~/path/to/how-to-eat-eggs.mp4
```

## A personal story: what I was making with FFmpeg

Years ago, I returned to work after 8 weeks on maternity leave. Each night I brought home pumped milk that needed to go direct into the fridge. One morning, not two weeks back to work, I walked down the stairs and looked over the kitchen counter to see a bag of milk jars left there the night before. Spoiled. I stood there for what felt like a long time, maybe a minute, overwhelmed by the mistake. When finally I accepted what I had done, I cried quietly over the sink while I poured the four little bottles into the drain.

My husband came down a little later and saw me by the sink. Quietly, he came over and wrapped me in a strong hug. Our kids followed and asked why I was crying. We all agreed we'd all help Mommy put the bottles away and each evening thereafter I was greeted by two cheerful little helpers racing to the door, eager to unpack the bottles and put them safely in the fridge.

I think the inspiration to record interviews came from another engineer who remarked that creators will always create, building a backlog of material that elevates their work to new mastery. As a parent, finding dedicated time to focus is near impossible. And we had an infant. I was searching for an outlet to connect two worlds: family and career.

Tracks like "How to Eat Eggs" ported me back to the day I recorded those moments--the joy I felt recording it. Beautiful, raw snapshots. We crafted something, and my kids weren't competing for my attention or distracting me, _they were at the core of my craft, giving it reason_. (If you're curious, you can find the interview project [here](/projects/how-to-eat-eggs/).) 

That's it. Did the install steps work for you? Could the steps be simpler or more secure? Please feel welcome to connect on Twitter or by email if you found something or have feedback to share, and thanks for reading! Keep making things!

## Resources

- [FFmpeg: Donate](https://ffmpeg.org/donations.html)
- [How to Set File Permissions Using `chmod`](https://www.washington.edu/computing/unix/permissions.html)
- [Bash: ./filename : Cannot execute binary file](https://askubuntu.com/questions/184280/bash-filename-cannot-execute-binary-file)
