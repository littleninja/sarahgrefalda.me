---
title: "Gradients with SVG"
description: "How to apply a gradient background on an SVG element"
categories: ["software development"]
tags: ["svg"]
date: 2021-02-01T21:57:27-08:00
draft: false
---

The exact phrasing was:

> [the] logo's gradient mesh doesn't translate as an SVG so it looks like we are stuck with this format [PNG].

Another developer stated something similar, that gradients couldn't be done with SVG. I love rising to a "you can't do it" challenge and SVGs are freaking amazing. YES! YES, you can do gradients, so let's see how! (CodePen [here](https://codepen.io/little_ninja/pen/vYyOxLm)).

## Our goal

By the end of this post, we hope to have an SVG that looks something like this:

![blue radial logo with oneworld wordmark](./oneworld.png)

If we decompose the parts, we have:

- a circle (`<circle>`)
- a radial gradient fill (`<radialGradient>`)
- some text with styling (`<text>`)

## Set the stage

Let's start with the circle and text.

```html
<svg height="64px" width="64px" role="img" viewBox="0 0 64 64">
  <circle cx="32" cy="32" r="30" stroke-width="2" stroke="white" fill="purple"></circle>
  <text x="6" y="36" stroke-width="0" fill="white">one</text>
  <text x="28" y="36" stroke-width="0" fill="white">world</text>
</svg>
<style>
```

Here is a 64 by 64 viewbox for us to draw in. A circle is centered (`cx="32" cy="32"`) and we've decreased the radius (`r="30"`) by 2 to adjust for the border stroke (`stroke-width="2"`). Honestly, tuning the text here wasn't as easy as I'm used to with HTML (maybe you know a better way in SVG). As a draft, though, this gets us a wordmark on the logo.

## Add a gradient

Now for the gradient. If you've worked with CSS backgrounds and gradients, this is similar. If you've used `radial-gradient()` and `linear-gradient()` in HTML and CSS, you can build similar `<radialGradient>` and `<linearGradient>` elements in SVG.

Our steps are:

1. Define a radial gradient element.
1. Reference the gradient element as a background image.

In an [earlier post](./usg-svg-sprites) we learned that a `<use>` element can pull in an SVG definition for icons just by referencing the id. We're going to apply something similar here!

```html
<svg height="64px" width="64px" role="img" viewBox="0 0 64 64">
  <def>
    <radialGradient id="OneworldGradient">
      <stop offset="0%" stop-color="#DEE1EE"/>
      <stop offset="100%" stop-color="#161182"/>
    </radialGradient>
  </def>
  <circle cx="32" cy="32" r="30" stroke-width="2" stroke="white" fill="url(#OneworldGradient)"></circle>
  <text x="6" y="36" stroke-width="0" fill="white">one</text>
  <text x="28" y="36" stroke-width="0" fill="white">world</text>
</svg>
<style>
```

Notice how we added a definition element and the radial gradient defined with an `id`? Next we reference it by `url(#id)` to draw it for the circle fill (background). Fantastic! We've started with the default centering the gradient, but we can off-center it by adjusting attributes: `cx`, `cy`, `r` for the end circle or where the radius stops (just like a circle!); and `fx`, `fy` for the start circle.

After twiddling with the start and end circle dimensions, text styling, and gradient fill color, it's not a perfect match but looking pretty similar:

![similar blue radial logo with oneworld wordmark](./oneworld-svg.png)

## More ideas

- [Linear gradients](https://developer.mozilla.org/en-US/docs/Web/SVG/Element/lineargradient), of course! Maybe the [Horizon meatball](https://blog.alaskaair.com/alaska-airlines/5-things-to-know-about-horizon-air/)?
- [Animated attributes??](https://developer.mozilla.org/en-US/docs/Web/SVG/Element/radialgradient#attributes) I can't imagine a use case but now I want one.
