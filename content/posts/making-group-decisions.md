---
title: "Making Group Decisions"
description: "Consensus is hard. As an individual contributor, here's what I've learned to make it easier."
categories: ["software development","personal development"]
tags: ["collaboration"]
date: 2021-03-23T21:33:02-07:00
draft: true
---

Isn't it awesome when you get to build something and it's the right thing?? And people love it??

And then there are days that are _not_ like that. Sometimes they're tangled in red tape, politics, and bureacracy. Yuck. Those aren't my favorite but I learned a few lessons that are helping me navigate it. These are a few practices for making better decisions as a group.

## Stop asking, start showing

Disclaimer: Here I'm talking about asking for _permission_ to do something. What I'm _not_ talking about is being curious, listening, and asking questions to understand a problem--that stuff is good!

Normally I'm like, "Hey designer! I made this thing, but I'm not sure what text you want here and what it needs to do on phones." Sometimes a designer prefer building to specification, we usually iron out the odd detail here and there. Other designers are comfortable starting with an idea, I'll draft a couple options and then we poke and nudge at it together.

I've noticed drafting options works well with product people. They appreciate having your information and seeing how you weighed the pros and cons--and they still get to decide. Other approaches range from giving only one option to "just the facts". Giving only one option gets a reaction like, "Hmm... just one option? Are you telling me everything?". It's fair: they're worried the facts or story will change and it hurts their credibility. The other extreme, dumping all the facts with no clear choices, puts the product person in an awkward position where they're probably not going to like or trust their decision. Structuring a few options with their risks and rewards gives your product person enough information to decide in confidence.

I find this practice useful when lots of options are present but folks aren't available to decide or are struggling to choose. It requires more work and in normal team situations can feel condescending so it's not my default.

- List the two or three strongest options.
- For each item, explain both why it's a great option and why you might not choose it.
- Bonus: build options that are iterations on each other. What's neat is this can become an opportunity for your product person to see the bigger picture you're building toward and for them to choose to become your ally on that quest. Presented as "good-and-better" as compared to split "either-or", the decider usually feels better about the decision, even if they go with just "good" for now.
- Skip garbage options--or if you need to mention it because it'll come up, explain why you wouldn't consider it.
- If pressed, go ahead and pitch the best candidate but don't get attached (this is _hard!_), be open to new information.

**Example**

Your parents visit on a weekend and no one can decide what to have for breakfast--classic situation for my family. Time to lay out some options!

- **Option 1: Stay in and make waffles.**
  Because crispy pockets of melted butter and hot syrup are the best breakfast food. They take a long time to make, though.
- **Option 2: Take-out breakfast burritos.**
  It's Saturday and our favorite taco bus is open today. Fast, simple, yummy, there's a burrito for everyone. Costs a bit more than homemade waffles but doesn't break the bank.
- **Option 3: Dine-out at the local diner.**
  They're open, too. No dishes and lots of options, maybe it's your favorite place when you're celebrating! You're worried about the bill, though, and you don't want to fuss about it.

- Not considered: cereal. Saving that for Wednesday afternoon.

## "Show me" and building a steel man argument for your opponent

One of the skills I focused on in 2020 was documenting and building visuals. My manager used this really great exercise where she started a doc and turned on comments and versioned revisions. It was the right level of structure so we could latch on and help her build something together.

I used the same technique myself recently. I kept explaining to my manager, "Choice A seems so obvious to me, but they're insisting on Choice B and it doesn't make sense." I wasn't sure if I wasn't explaining well or if I was missing something important in their view and--bing!--that was the light bulb! I drafted a document that explained their hypothesis and test procedure, and I added the same for mine. This approach is sometimes called the "steel man argument"--the inverse of the strawman fallacy, and it _did_ show me some concerns I was struggling to see before. In an exciting and unexpected twist, I invited comments and realized we weren't so far off: "I'm on-board with this idea, I just want to make sure our product people have a baseline first."

## "Explain it back to me?"

I've heard this but still need practice. When a visual isn't feasible, the verbal version is asking the other person to explain what will happen next, or explain what the solution looks like. Some examples including my go-to from Brene Brown's "Daring to Lead":

- "Can you paint for me what 'done' looks like here?"
- "I think I hear agreement. Can I check: can you tell me what the next steps look like?"

I still playing with the right way to ask without triggering a defense response, and trying to figure out how to tactfully test for "echo" responses. I'm still working out when to do this (too late and you risk wasting the whole meeting), how to ask, and how to tactfully spot differences.


## Love your ally--and your opposition

If you're lucky, you have someone in your corner who gets what you're fighting for and wants to see you win. It's pretty easy to vent, but more important: let that person know you appreciate them!

The next level is also sharing gratitude with your "opposition". You might be mad and frustrated: this is all taking _waaaay_ too long and more effort than it should (because that's how I usually feel). Choosing to say "thank you" for the feedback and sticking with it through tough decisions is how we choose to create safe spaces for ourselves and our peers.

It's so friggin' hard--and don't do it if it's not authentic. Start with the easier apprecation for the folks who listen to you, challenge you, and especially stick their necks out for you. Next, where I need to grow: extending gratitude--in a genuine way!--to folks who might seem like the source of the pain but who raise good questions, offer to help (even when it creates more work), and in this way build bridges for future work.
