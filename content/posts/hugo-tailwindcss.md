---
title: "Hugo and Tailwind CSS"
description: "Switching my blog styles to use Tailwind CSS and PostCSS pipes"
categories: ["software development"]
tags: ["hugo","tailwindcss","hugo-pipes"]
date: 2021-01-31T21:19:29-08:00
draft: true
---

```shell
Error: Error building site: POSTCSS: failed to transform "css/styles.css" (text/css): "/Users/sarah/Projects/sarahgrefalda/themes/cabbage/assets/css/styles.css:13:1": CssSyntaxError: /Users/sarah/Projects/sarahgrefalda/stdin:13:26: The `dark:text-indigo-200` class does not exist, but `2xl:text-indigo-200` does. If you're sure that `dark:text-indigo-200` exists, make sure that any `@import` statements are being properly processed before Tailwind CSS sees your CSS, as `@apply` can only be used for classes in the same CSS tree.
```

- what **version** was tailwind (src)? what version is tailwind (assets)?
  local: 2.0.2
  published: 2.0.2
- what **size** is styles.css?
  local: 3.74MB
  published (src): 2.70KB

## dependencies

### basics

- postcss
- postcss-cli
- tailwindcss
- autoprefixer

### [Dev.to](https://dev.to/divrhino/how-to-add-tailwindcss-to-your-hugo-site-5290)

- postcss-import: resolve paths of `@import` rules

### [Bud Parr, Ananke maintainer](https://github.com/budparr/hugopipes-tailwindcss)

- postcss-import
- @fullhuman/postcss-purgecss

copied this postcss.js (config)

### [Another GitHub repo](https://github.com/bep/hugo-starter-tailwind-basic)

- add build param `writeStats = true` which is also in the Hugo PostProcess docs. This instructs hugo to output a json structure of html elements: tags, classes, ids.

---

With Bud Parr's configuration, dark styles work but it's the whole unparsed styles (5.95MB). Noteably Parr's changes added the postcss-import module in the postcss config (dark works).

Now to fix unpurged CSS:

https://gohugo.io/hugo-pipes/postprocess/#css-purging-with-postcss

As I think Parr's README pointed out, Tailwind's abstraction on PurgeCSS doesn't seem to work. Configuring PurgeCSS directly in the postcss config does, though. 

Another weird quirk: SVGs were broken and publish date didn't format. Remembered a comment again by Parr and removed any and all empty template files. Footer SVGs and publish date renders correctly.

Production styles are 8.44KB, development styles will be full 5+MB.	
