---
title: "sarahgrefalda"
description: "projects and writing"
featured_image: ""
---

Welcome, I'm Sarah. I write about what I'm learning: web development and processes in software development. As I play to learn you'll find projects here, too. This is the short intro, you can learn more about me [here](/about).
